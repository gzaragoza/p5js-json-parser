//data source
//https://data2.unhcr.org/en/country/esp

let totalArrivals = 0;
let totalDeadAndMissing = 0;

function setup() {
  createCanvas(window.innerWidth, window.innerHeight);
  background(55);

  textSize(18);
  //Load json data
  loadData();
}

function draw() {
  // put drawing code here
}

function loadData() {
  // Load the JSON from file
  loadedJSON = loadJSON(
    "https://data2.unhcr.org/population/?widget_id=240498&geo_id=729&population_group=4797,4798&year=latest",
    onDataload
  );
}

function onDataload(data) {
  console.log(data.data[0].individuals);
  //extract number of individuals arrived
  totalArrivals = data.data[0].individuals;

  paintVisualization();
}

function paintVisualization() {
  text("Total of arrivals of refugees to Spain: " + totalArrivals, 20, 20);

  for (i = 0; i < totalArrivals; i++) {
    // Math.random() Retorna un número aleatorio entre 0 (incluido) y 1 (excluido)
    const randomX = Math.random() * window.innerWidth + 1;
    const randomY = Math.random() * window.innerHeight + 1;

    ellipse(randomX, randomY, 15);
  }
}
